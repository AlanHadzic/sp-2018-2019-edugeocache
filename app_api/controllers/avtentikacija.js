var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.registracija = function(zahteva, odgovor) {
  if (!zahteva.body.ime || !zahteva.body.elektronskiNaslov || !zahteva.body.geslo) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  var uporabnik = new Uporabnik();
  uporabnik.ime = zahteva.body.ime;
  uporabnik.elektronskiNaslov = zahteva.body.elektronskiNaslov;
  uporabnik.nastaviGeslo(zahteva.body.geslo);
  uporabnik.save(function(napaka) {
   if (napaka) {
     vrniJsonOdgovor(odgovor, 500, napaka);
   } else {
     vrniJsonOdgovor(odgovor, 200, {
       "zeton": uporabnik.generirajJwt()
     });
   }
  });
};

module.exports.prijava = function(zahteva, odgovor) {
  if (!zahteva.body.elektronskiNaslov || !zahteva.body.geslo) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  passport.authenticate('local', function(napaka, uporabnik, podatki) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if (uporabnik) {
      vrniJsonOdgovor(odgovor, 200, {
        "zeton": uporabnik.generirajJwt()
      });
    } else {
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};