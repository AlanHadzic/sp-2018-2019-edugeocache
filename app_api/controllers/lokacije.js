var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.lokacijeSeznamPoRazdalji = function(zahteva, odgovor) {
  var lng = parseFloat(zahteva.query.lng);
  var lat = parseFloat(zahteva.query.lat);
  var razdalja = parseFloat(zahteva.query.maxRazdalja);
  razdalja = isNaN(razdalja) ? 20 : razdalja;
  var stZadetkov = 10;
  if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Parametra lng in lat sta obvezna."
    });
    return;
  }
  Lokacija
    .aggregate([{
      $geoNear: {
        near: {
          type: "Point",
          coordinates: [lng, lat]
        },
        maxDistance: razdalja * 1000,
        key: "coords",
        distanceField: "razdalja",
        spherical: true,
        query: "find()"
      }
    }])
    .limit(stZadetkov)
    .exec(function(napaka, rezultati) {
      var lokacije = [];
      if (napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
        rezultati.forEach(function(dokument) {
          lokacije.push({
            razdalja: dokument.razdalja / 1000,
            naziv: dokument.naziv,
            naslov: dokument.naslov,
            ocena: dokument.ocena,
            lastnosti: dokument.lastnosti,
            _id: dokument._id
          });
        });
        vrniJsonOdgovor(odgovor, 200, lokacije);
      }
    });
};

module.exports.lokacijeKreiraj = function(zahteva, odgovor) {
  Lokacija.create({
    naziv: zahteva.body.naziv,
    naslov: zahteva.body.naslov,
    lastnosti: zahteva.body.lastnosti.split(","),
    koordinate: [
      parseFloat(zahteva.body.lng), 
      parseFloat(zahteva.body.lat)
    ],
    delovniCas: [{
      dnevi: zahteva.body.dnevi1,
      odprtje: zahteva.body.odprtje1,
      zaprtje: zahteva.body.zaprtje1,
      zaprto: zahteva.body.zaprto1
    }, {
      dnevi: zahteva.body.dnevi2,
      odprtje: zahteva.body.odprtje2,
      zaprtje: zahteva.body.zaprtje2,
      zaprto: zahteva.body.zaprto2
    }]
  }, function(napaka, lokacija) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, lokacija);
    }
  });
};

module.exports.lokacijePreberiIzbrano = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije) {
    Lokacija
      .findById(zahteva.params.idLokacije)
      .exec(function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": 
              "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, lokacija);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, { 
      "sporočilo": "Manjka enolični identifikator idLokacije!"
    });
  }
};

module.exports.lokacijePosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idLokacije) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije, idLokacije je obvezen parameter"
    });
    return;
  }
  Lokacija
    .findById(zahteva.params.idLokacije)
    .select('-komentarji -ocena')
    .exec(
      function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem lokacije."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        lokacija.naziv = zahteva.body.naziv;
        lokacija.naslov = zahteva.body.naslov;
        lokacija.lastnosti = zahteva.body.lastnosti.split(",");
        lokacija.koordinate = [
          parseFloat(zahteva.body.lng), 
          parseFloat(zahteva.body.lat)
        ],
        lokacija.delovniCas = [{
          dnevi: zahteva.body.dnevi1,
          odprtje: zahteva.body.odprtje1,
          zaprtje: zahteva.body.zaprtje1,
          zaprto: zahteva.body.zaprto1
        }, {
          dnevi: zahteva.body.dnevi2,
          odprtje: zahteva.body.odprtje2,
          zaprtje: zahteva.body.zaprtje2,
          zaprto: zahteva.body.zaprto2
        }];
        lokacija.save(function(napaka, lokacija) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, lokacija);
          }
        });
      }
    );
};

module.exports.lokacijeIzbrisiIzbrano = function(zahteva, odgovor) {
  var idLokacije = zahteva.params.idLokacije;
  if (idLokacije) {
    Lokacija
      .findByIdAndRemove(idLokacije)
      .exec(
        function(napaka, lokacija) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije, idLokacije je obvezen parameter."
    });
  }
};